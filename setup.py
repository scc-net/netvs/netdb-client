from setuptools import setup
from setuptools.command.build_py import build_py
import subprocess
import os
import requests
import semver
        
vers = requests.get(f"{os.environ.get('NETDB_SCHEME', 'https')}://{os.environ.get('NETDB_ENDPOINT')}").json()[0]
largest_ver = None
for v in vers:
    sem_v = semver.Version.parse(v['numeric'])
    if largest_ver is None or largest_ver < sem_v:
        largest_ver = sem_v 

latest_previous_util_version = os.environ.get('LATEST_UTIL_VERSION', None)
latest_previous_api_version = os.environ.get('LATEST_API_VERSION', None)
post_version = os.environ.get('POST_NUM', None)
if post_version is not None and not post_version.isdigit():
    post_version = None

def get_gen_version():
    ver = subprocess.run(["pip freeze | grep net_api_generator"], shell=True, text=True, check=True, capture_output=True).stdout
    return ver.split('@')[-1][:12]

class APIGenBuild(build_py):
    def run(self):
        build_versions = [f'{v["major"]}.{v["minor"]}' for v in vers]
        default_version = f'{largest_ver.major}.{largest_ver.minor}'
        environ = os.environ.copy()
        for version in build_versions:
            target_dir = os.path.join(self.build_lib, 'netdb_client', f"api{version.replace('.', '')}")
            self.mkpath(target_dir)
            environ['NETDB_VERSION'] = version
            subprocess.run(['net-api-generator', 'python', f'--output-dir={target_dir}'], check=True, env=environ)
        target_dir = os.path.join(self.build_lib, 'netdb_client')
        environ['NETDB_VERSION'] = default_version
        subprocess.run(['net-api-generator', 'python', f'--output-dir={target_dir}'], check=True, env=environ)
        build_py.run(self)

util_version = f'{os.environ.get("CI_COMMIT_SHORT_SHA", "HEAD")}.{get_gen_version()}'

if util_version != latest_previous_util_version and str(largest_ver) == latest_previous_api_version:
    if post_version is None:
        post_version = 1
    else:
        post_version = int(post_version) + 1

setup(
    name='netdb_client',
    version=f'{str(largest_ver)}{".post" + str(post_version) if post_version is not None else ""}{".dev1" if os.environ.get("CI_COMMIT_BRANCH", "local") == "devel" else ""}+{util_version}',
    author='NETVS-Team <netvs@scc.kit.edu>',
    description='This is a meta package to install the automatically generated NET-API definitions for the currently supported API versions.',
    url='https://gitlab.kit.edu/scc-net/netvs/netdb-client',
    install_requires=['requests'],
    python_requires='>=3.7',
    packages=['netdb_client'],
    cmdclass={'build_py': APIGenBuild}
)

