"""netdb_client helper functions for configuration and response-parsing"""
import argparse
import configparser
import os
import stat


class ArgumentParser(argparse.ArgumentParser):
    """Argument parser with default common arguments for NetDB-api cli
    tools. Includes default values."""

    def __init__(
            self,
            epilog="The Variables BASE_URL, VERSION and TOKEN can be loaded from config file,"
                   "environment or from command line arguments.\n"
                   "Variable precendence is as followed (from greatest to least,"
                   "means the first listed variable overwrites all other variables):\n"
                   "  1. cli-arguments\n"
                   "  2. environment variables\n"
                   "  3. config-file\n",
            formatter_class=argparse.RawTextHelpFormatter,
            **kwargs
    ):
        super().__init__(formatter_class=formatter_class, epilog=epilog, **kwargs)
        self.add_argument('--auth-config',
                          default=os.path.expanduser('~/.config/netdb_client.ini'),
                          help='config file path (default: %(default)s)')
        self.add_argument('--endpoint', '-e',
                          help='endpoint to use.\n'
                               'Environment: "NETDB_ENDPOINT"\n'
                               'Config: [DEFAULT]: endpoint')
        self.add_argument('--base-url', '-b',
                          help='webapi server.\n'
                               'Environment: "NETDB_BASE_URL"\n'
                               'Config: [$endpoint]: base_url')
        self.add_argument('--token', '-t',
                          help='user API token.\n'
                               'Environment: "NETDB_TOKEN"\n'
                               'Config: [$endpoint]: token')

    def parse_args(self, args=None, namespace=None):
        args = super().parse_args(args, namespace)
        config = configparser.ConfigParser()
        configpath = os.path.expanduser(args.auth_config)
        if os.path.isfile(configpath):
            if os.stat(configpath).st_mode & stat.S_IRWXO:
                self.error(f"Config file is readable by others. Please set it at least to 600 but never other-readable.")
            config.read(configpath)

        def load_config(option):
            """load config for an option"""

            # if value is already set, we can stop here
            if getattr(args, option) is not None:
                return

            # try to load option from environment
            value = os.getenv(f'NETDB_{option.upper()}', None)

            if value is None:
                # endpoint should be loaded from DEFAULT section
                if option == 'endpoint':
                    section = 'DEFAULT'
                # everything else loads from the $endpoint section
                else:
                    section = getattr(args, 'endpoint')
                # load value from config section
                value = config.get(section, option, fallback=None)

            # For backwards compatibility:
            # if base_url is still unknown use endpoint instead
            if option == 'base_url' and value is None:
                value = section

            if value is None:
                self.error(
                    f'No {option} specified (looked in args, environment variable '
                    f'"NETDB_{option.upper()}", config-file in "{args.auth_config} section {section}")')

            # we have a value now and can use it
            setattr(args, option, value)

        # start with endpoint to get the right config section
        for option in ['endpoint', 'base_url', 'token']:
            load_config(option)

        return args


def list_to_generator_map_one2one(array, key_name):
    """Mapping function to convert list of dicts to a mapping with value as key for each dict
    (1 to 1 Mapping). Returns a generator object."""
    return ((item[key_name], item) for item in array)


def list_to_generator_map_one2many(array, key_name):
    """Mapping function to convert list of dicts to a mapping with values as key for each lists
     (1 to n Mapping). Returns a generator object."""
    res = {}
    for item in array:
        if not item[key_name] in res:
            res[item[key_name]] = []
        res[item[key_name]].append(item)
    for key, value in res.items():
        yield key, value
