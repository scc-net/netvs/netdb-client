# NETDB API Client Library for Python

## Install

The library can be installed from our python-registry:
```
pip install netdb-client --index-url https://gitlab.kit.edu/api/v4/groups/198806/-/packages/pypi/simple
```

The library supports the last 3 NETDB-API releases (release, oldrelease, deprecated). The version of the library is defined by the newest supported API version.

If not explicitly specified the most recent supported version will be used. Examples for using an older API version are listed in section "Usage".

There are also development build against the NETDB-Devel-API (api.netdb-devel.scc.kit.edu). 

## Usage
### Init
Initialisation works as follows, for configuration an `APIEndpoint`-Instance (in this case the test instance) has to be passed to the `API-Session` call.
```python
>>> import netdb_client.api40

>>> endpoint = netdb_client.APIEndpoint(
    base_url='api.netdb-test.scc.kit.edu',
    token='<token>'
)
>>> api = netdb_client.api40.APISession(endpoint)
```

`util` provides an `ArgumentParser`-Class which can be used for configuration and config loading. This class has been
implemented transparent, so you can extend the arguments or functionality as needed by your application or script.
The functionality has been extended, so the `parse_args()` method checks for environment variables and an config file in "ini"-format.
Variable precendence is as followed (from greatest to least, means the first source overwrites all other):
  1. cli-arguments
  2. environment variables
  3. config-file (default path: `~/.config/netdb_client.ini`)
```python
>>> import netdb_client.util

>>> parser = netdb_client.util.ArgumentParser(description='Beispielbeschreibung für Skript.')
>>> args = parser.parse_args()
>>> endpoint = netdb_client.APIEndpoint(**vars(args))
>>> api = netdb_client.api40.APISession(endpoint)
```
Example help (`h` / `--help`) output:
```
usage: test.py [-h] [--auth-config AUTH_CONFIG] [--endpoint ENDPOINT] [--base-url BASE_URL] [--version VERSION] [--token TOKEN]

Argument Parser for netdb_client

optional arguments:
  -h, --help            show this help message and exit
  --auth-config AUTH_CONFIG
                        config file path (default: /home/user/.config/netdb_client.ini)
  --endpoint ENDPOINT, -e ENDPOINT
                        endpoint to use.
                        Environment: "NETDB_ENDPOINT"
                        Config: [DEFAULT]: endpoint
  --base-url BASE_URL, -b BASE_URL
                        webapi server.
                        Environment: "NETDB_BASE_URL"
                        Config: [$endpoint]: base_url
  --token TOKEN, -t TOKEN
                        user API token.
                        Environment: "NETDB_TOKEN"
                        Config: [$endpoint]: token

The Variables BASE_URL, VERSION and TOKEN can be loaded from config file,environment or from command line arguments.
Variable precendence is as followed (from greatest to least,means the first listed variable overwrites all other variables):
  1. cli-arguments
  2. environment variables
  3. config-file
```

### Config file
An example config can be found in this repo (`example_config.ini`). Be aware to set the file mode of the config at least to `0600` but never other-readable.

## Migration
### API 3.2 -> API 4.0
See the [API 4.0 Release Notes](https://doku.netdb.scc.kit.edu/webapi/4.0/relnotes/).

## Examples
More examples can be found in our repository [net-api-python-scripts](https://gitlab.kit.edu/scc-net/net-api-python-scripts) where we store some handy scripts for our daily work and some time-consuming tasks.
### Basic queries
List all BCDs:
```python
>>> import netdb_client.api40.nd
>>> netdb_client.api40.nd.Bcd.list(api_session=api)
[Bcd(name='scc-net-web-1', categ='USER', is_own=True, log_pk=13270200, seclvl=0, vlan_count=1, description='[scc-net-web/1]: Webserver von SCC-NET / [ipv6-scc-net-web/1]: Webserver von SCC-NET *** ehemalige Bereichsinfo ***: [scc-net-web/1]: Webserver von SCC-NET / [ipv6-scc-net-web/1]: Webserver von SCC-NET', dhcp_enabled=False, subnet_count=2, ou_short_name='SCC-NET', dhcp_ttl_value=600, admin_description=None, dhcp_accept_new_leases=False, dhcp_leasetime_dyn_days=0, dhcp_leasetime_dyn_hours=2, dhcp_leasetime_dyn_minutes=0, dhcp_leasetime_static_days=0, dhcp_leasetime_static_hours=6, dhcp_offer_rsv_time_minutes=0, dhcp_offer_rsv_time_seconds=30, dhcp_leasetime_static_minutes=0, dhcp_accept_requested_hostname=True)]
```

List all CNAME-Records for given BCD:
```python
>>> import netdb_client.api40.dns
>>> netdb_client.api40.dns.Record.list(api_session=api, type_old="CNAME", fqdn_list_old=["netvs-devel.scc.kit.edu"])
[..., Record(ttl=None, data='net-web09.scc.kit.edu.', fqdn='netvs-devel.scc.kit.edu.', type='CNAME', zone='kit.edu.', is_own=True, fqdn_type='alias', host_is_nws=False, target_fqdn='net-web09.scc.kit.edu.', target_ipaddr=None, ttl_reset_date=None, fqdn_description=None, target_fqdn_type='host', ttl_zone_default=3600, target_data_unref=None, target_is_singleton=True, target_is_reverse_unique=False), ...]
```

### Advanced queries
List BCDs with their Subnets:
```python
>>> import netdb_client.api40.nd
>>> api.execute_ta([
    netdb_client.nd.api40.Bcd.list_ta(), # == {"name": "nd.bcd.list"}
    {"name": "nd.ip_subnet.list", "inner_join_ref": {0: "default"}},
])
[[{'name': 'scc-net-web-1', ...}], [{'bcd': 'scc-net-web-1', ...}, ...]]
```


